const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.json([
        {
            // title: "Über uns",
            text:"Gaming neu gedacht – wir bringen Retro Klassiker mit unseren handgefertigten Arcade-Automaten und eigener Software auf das nächste Level. Zudem stellen wir eine Plattform für aufstrebende Entwickler bereit, die ihre Spiele veröffentlichen möchten. Unsere Automaten bieten mehr als nur Retro-Charme. Mit zusätzlichen Features wie Online-Bestenlisten, individuellen Designs und Qualität „Made in Germany“ setzen wir neue Maßstäbe im Arcade-Gaming."   
        },
	{
            title: "Mission",
            text:"Wir wollen jedem die Chance geben klassische Arcade Games zu erleben. Ob für einen nostalgischen Rückblick oder eine kleine Pause zwischendurch - bei uns findet jeder sein Spiel. Dabei sind wir natürlich nicht allein - unser Unternehmen wird vom Gründungsnetzwerk Saxeed, R42, und der Studentenwerkstadt der TU Chemnitz unterstützt. Außerdem werden wir von futureSax und der Sächsischen Aufbaubank mit der Gründungsförderung \„InnoStartBonus\" gefördert."   
	},
    ]);
  });
  

module.exports = router;
