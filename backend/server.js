const express = require('express');
const cors = require('cors');
const path = require('path');
const news = require('./news.js');
const personen = require('./personen.js');
const text = require('./text.js');
const config = require('./config/development.js');


const app = express();

app.use(cors({
  origin: config.corsOrigins,
}));

app.use(`/${config.staticImageDirectory}`, express.static(path.join(__dirname, config.staticImageDirectory)));
app.use(`/${config.staticFontDirectory}`, express.static(path.join(__dirname, config.staticFontDirectory)));

app.use('/news', news);
app.use('/personen', personen);
app.use('/text', text);

app.listen(config.port, () => {
  console.log(`Server is running on port ${config.port}`);
});
