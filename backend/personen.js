const express = require('express');
const router = express.Router();
const config = require('./config/development.js');

router.get('/', (req, res) => {
  res.json([
    {
      id: 1,
      name: 'Niki Hendel',
      bildUrl: config.baseUrl + '/images/NikiHendel.png',
      lebensabschnitt: 'Anwendungsentwickler',
    },
    {
      id: 2,
      name: 'Partner',
      bildUrl: config.baseUrl + '/images/auotmat-full.png',
      lebensabschnitt: 'FutureSAX',
    },
    {
      id: 5,
      name: 'Alexander Stiwi',
      bildUrl: config.baseUrl + '/images/AlexanderStiwi.png',
      lebensabschnitt: 'Anwendungsentwickler',
    },
    {
      id: 3,
      name: 'Patrick Scheps',
      bildUrl: config.baseUrl + '/images/PatrickSchepser.png',
      lebensabschnitt: 'Modellbau',
    },
    {
      id: 4,
      name: 'Larissa Flade',
      bildUrl: config.baseUrl + '/images/lara.png',
      lebensabschnitt: 'Grafikdesignerin',
    },
  ]); 
});

module.exports = router;

