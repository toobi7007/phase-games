//news.js
const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.json([
      {
      //   title: "Unternehmen",
      //   text:"Unser Unternehmen konnte sich ebenso bereits schon wichtige Partner für die Zusammenarbeit sichern wie das Gründungsnetzwerk Saxeed und das R42 in Leipzig. In der Schüler- und Studierendenwerkstatt der TU Chemnitz entwerfen und bauen wir derzeit unseren Prototypen, ebenso unterstützen uns futureSax und die Sächsische Aufbaubank mit der Gründungsförderung „Innostartbonus“. „Diese Maßnahme wird mitfinanziert mit Steuermitteln auf Grundlage des vom sächsischen Landtag beschlossenen Haushaltes.“"   },
      // {
      //   title: "Phase Games",
      //   text: "Wir von Phase Games haben die Vision, dass ‚Zocken‘ wieder zu einem sozialen und gesellschaftlichen Event wird. Egal welches Alter man hat, ob man Nostalgiker ist oder sich einfach nur 5 Minuten vertreiben muss bis zum nächsten Termin, wir wollen alle erreichen. Unsere Idee ist es mit selbstgebauten Arcade-Automaten und Software in Eigenregie einerseits jenes zu vertreiben, sowie andererseits mittels des Automaten eine Plattform für (Hobby-) Spieleentwickler auf aller Welt geben. Diese Automaten werden auch über einige extra Features verfügen im Vergleich zu ihren Retro-Vorgängern wie beispielsweise Online-Bestenlisten (Stadt, Land etc.), sowie individuelle Designs und das Made in Germany." 
      },
    ]);
  });
  

module.exports = router;
