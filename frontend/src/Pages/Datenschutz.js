import '../fonts/01_PressStart2P-Regular.ttf';
import '../styles/Datenschutz.css'; 
import '../fonts/01_PressStart2P-Regular.ttf';
const config = require('../config/development.js');

export default function DA() {
 const backgroundImageUrl = `${config.baseUrl}/images/body.png`; 
    
  return (
    <div> 
    
      <div className="container_info" style={{ backgroundImage: `url(${backgroundImageUrl})` }}>
        <div className='single_container'>
        <div className='Textbox'>
        <h1>Datenschutzerklärung</h1>
        <h2>Allgemeiner Hinweis und Pflichtinformationen</h2>
        <h3>Benennung der verantwortlichen Stelle</h3>
        <p>Die verantwortliche Stelle für die Datenverarbeitung auf dieser Website ist:</p>
        <p><span id="s3-t-firma">PHASE Games</span><br/><span id="s3-t-ansprechpartner">Niki Hendel</span><br/><span id="s3-t-strasse">Beethovenstr. 7</span><br/><span id="s3-t-plz">Chemn</span> <span id="s3-t-ort">Chemnitz</span></p>
        <p>Die verantwortliche Stelle entscheidet allein oder gemeinsam mit anderen über die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten (z.B. Namen, Kontaktdaten o. Ä.).</p>
        <h3>Widerruf Ihrer Einwilligung zur Datenverarbeitung</h3>
        <p>Nur mit Ihrer ausdrücklichen Einwilligung sind einige Vorgänge der Datenverarbeitung möglich. Ein Widerruf Ihrer bereits erteilten Einwilligung ist jederzeit möglich. Für den Widerruf genügt eine formlose Mitteilung per E-Mail. Die Rechtmäßigkeit der bis zum Widerruf erfolgten Datenverarbeitung bleibt vom Widerruf unberührt.</p>
        <h3>Recht auf Beschwerde bei der zuständigen Aufsichtsbehörde</h3>
        <p>Als Betroffener steht Ihnen im Falle eines datenschutzrechtlichen Verstoßes ein Beschwerderecht bei der zuständigen Aufsichtsbehörde zu. Zuständige Aufsichtsbehörde bezüglich datenschutzrechtlicher Fragen ist der Landesdatenschutzbeauftragte des Bundeslandes, in dem sich der Sitz unseres Unternehmens befindet. Der folgende Link stellt eine Liste der Datenschutzbeauftragten sowie deren Kontaktdaten bereit: <a href="https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html" target="_blank">https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html</a>.</p>
        <h3>Recht auf Datenübertragbarkeit</h3>
        <p>Ihnen steht das Recht zu, Daten, die wir auf Grundlage Ihrer Einwilligung oder in Erfüllung eines Vertrags automatisiert verarbeiten, an sich oder an Dritte aushändigen zu lassen. Die Bereitstellung erfolgt in einem maschinenlesbaren Format. Sofern Sie die direkte Übertragung der Daten an einen anderen Verantwortlichen verlangen, erfolgt dies nur, soweit es technisch machbar ist.</p>
        <h3>Recht auf Auskunft, Berichtigung, Sperrung, Löschung</h3>
        <p>Sie haben jederzeit im Rahmen der geltenden gesetzlichen Bestimmungen das Recht auf unentgeltliche Auskunft über Ihre gespeicherten personenbezogenen Daten, Herkunft der Daten, deren Empfänger und den Zweck der Datenverarbeitung und ggf. ein Recht auf Berichtigung, Sperrung oder Löschung dieser Daten. Diesbezüglich und auch zu weiteren Fragen zum Thema personenbezogene Daten können Sie sich jederzeit über die im Impressum aufgeführten Kontaktmöglichkeiten an uns wenden.</p>
        <h3>SSL- bzw. TLS-Verschlüsselung</h3>
        <p>Aus Sicherheitsgründen und zum Schutz der Übertragung vertraulicher Inhalte, die Sie an uns als Seitenbetreiber senden, nutzt unsere Website eine SSL-bzw. TLS-Verschlüsselung. Damit sind Daten, die Sie über diese Website übermitteln, für Dritte nicht mitlesbar. Sie erkennen eine verschlüsselte Verbindung an der „https://“ Adresszeile Ihres browsers und am Schloss-Symbol in der browserzeile.</p>

        <h2>Datenschutzbeauftragter</h2>
        <p>Wir haben einen Datenschutzbeauftragten bestellt.</p>
        <p>Niki, Hendel<br/>Beethovenstr 7<br/>09130 Chemnitz</p>
        <p>Telefon: 49 172 4567 402<br/>E-Mail: Hendel33333@gmail.com</p>


        <h2>Server-Log-Dateien</h2>
        <p>In Server-Log-Dateien erhebt und speichert der Provider der Website automatisch Informationen, die Ihr browser automatisch an uns übermittelt. Dies sind:</p>
        <ul>
          <li>
            <p>Besuchte Seite auf unserer Domain</p>
          </li>
          <li>
            <p>Datum und Uhrzeit der Serveranfrage</p>
          </li>
          <li>
            <p>browsertyp und browserversion</p>
          </li>
          <li>
            <p>Verwendetes Betriebssystem</p>
          </li>
          <li>
            <p>Referrer URL</p>
          </li>
          <li>
            <p>Hostname des zugreifenden Rechners</p>
          </li>
          <li>
            <p>IP-Adresse</p>
          </li>
        </ul>
        <p>Es findet keine Zusammenführung dieser Daten mit anderen Datenquellen statt. Grundlage der Datenverarbeitung bildet Art. 6 Abs. 1 lit. b DSGVO, der die Verarbeitung von Daten zur Erfüllung eines Vertrags oder vorvertraglicher Maßnahmen gestattet.</p>


        <h2>Kontaktformular</h2>
        <p>Per Kontaktformular übermittelte Daten werden einschließlich Ihrer Kontaktdaten gespeichert, um Ihre Anfrage bearbeiten zu können oder um für Anschlussfragen bereitzustehen. Eine Weitergabe dieser Daten findet ohne Ihre Einwilligung nicht statt.</p>
        <p>Die Verarbeitung der in das Kontaktformular eingegebenen Daten erfolgt ausschließlich auf Grundlage Ihrer Einwilligung (Art. 6 Abs. 1 lit. a DSGVO). Ein Widerruf Ihrer bereits erteilten Einwilligung ist jederzeit möglich. Für den Widerruf genügt eine formlose Mitteilung per E-Mail. Die Rechtmäßigkeit der bis zum Widerruf erfolgten Datenverarbeitungsvorgänge bleibt vom Widerruf unberührt.</p>
        <p>Über das Kontaktformular übermittelte Daten verbleiben bei uns, bis Sie uns zur Löschung auffordern, Ihre Einwilligung zur Speicherung widerrufen oder keine Notwendigkeit der Datenspeicherung mehr besteht. Zwingende gesetzliche Bestimmungen - insbesondere Aufbewahrungsfristen - bleiben unberührt.</p>


        <h2>Cookies</h2>
        <p>Unsere Website verwendet Cookies. Das sind kleine Textdateien, die Ihr Webbrowser auf Ihrem Endgerät speichert. Cookies helfen uns dabei, unser Angebot nutzerfreundlicher, effektiver und sicherer zu machen.</p>
        <p>Einige Cookies sind “Session-Cookies.” Solche Cookies werden nach Ende Ihrer browser-Sitzung von selbst gelöscht. Hingegen bleiben andere Cookies auf Ihrem Endgerät bestehen, bis Sie diese selbst löschen. Solche Cookies helfen uns, Sie bei Rückkehr auf unserer Website wiederzuerkennen.</p>
        <p>Mit einem modernen Webbrowser können Sie das Setzen von Cookies überwachen, einschränken oder unterbinden. Viele Webbrowser lassen sich so konfigurieren, dass Cookies mit dem Schließen des Programms von selbst gelöscht werden. Die Deaktivierung von Cookies kann eine eingeschränkte Funktionalität unserer Website zur Folge haben.</p>
        <p>Das Setzen von Cookies, die zur Ausübung elektronischer Kommunikationsvorgänge oder der Bereitstellung bestimmter, von Ihnen erwünschter Funktionen (z.B. Warenkorb) notwendig sind, erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Als Betreiber dieser Website haben wir ein berechtigtes Interesse an der Speicherung von Cookies zur technisch fehlerfreien und reibungslosen Bereitstellung unserer Dienste. Sofern die Setzung anderer Cookies (z.B. für Analyse-Funktionen) erfolgt, werden diese in dieser Datenschutzerklärung separat behandelt.</p>
        </div>
        </div>
      </div>
    </div>
  );
}