# Installation der Umgebung

Klonen des Projekts:

```
git clone https://gitlab.com/toobi7007/phase-games
```


Anschliessend die Images bauen:

```
sudo docker compose build
```

Zum ausfuehren die Umgebung starten:

```
sudo docker compose up
```

Man kann die Option -d verwenden, um die Ausfuehrung in den Hintergrund zu verlegen.

Zum Beenden der Container:

```
sudo docker compose stop
```
